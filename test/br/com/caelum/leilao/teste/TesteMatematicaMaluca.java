package br.com.caelum.leilao.teste;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.caelum.matematica.MatematicaMaluca;

public class TesteMatematicaMaluca {

	@Test
	public void numeroMaiorQueTrinta() {
		MatematicaMaluca matematica = new MatematicaMaluca();
		
		assertEquals(35 * 4, matematica.contaMaluca(35));
	}
	
	@Test
	public void numeroMaiorQueDez() {
		MatematicaMaluca matematica = new MatematicaMaluca();
		
		assertEquals(15 * 3, matematica.contaMaluca(15));
	}
	
	@Test
	public void numeroMenorIgualDez() {
		MatematicaMaluca matematica = new MatematicaMaluca();
		
		assertEquals(5 * 2, matematica.contaMaluca(5));
	}
}
