package br.com.caelum.leilao.teste;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import br.com.caelum.filtro.FiltroDeLances;
import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Usuario;

public class TesteFiltroDeLances {

	@Test
	public void deveSelecionaLancesEntre1000E3000() {
		Usuario joao = new Usuario("joao");
		
		FiltroDeLances filtro = new FiltroDeLances();
		
		List<Lance> resultado = filtro.filtra(Arrays.asList(
												new Lance(joao, 2000),
												new Lance(joao, 1000),
												new Lance(joao, 3000),
												new Lance(joao, 800)));
		
		assertEquals(1, resultado.size());
		assertEquals(2000, resultado.get(0).getValor(), 0.00001);
	}
	
	@Test
	public void deveSelecionaLancesEntre500E700() {
		Usuario joao = new Usuario("joao");
		
		FiltroDeLances filtro = new FiltroDeLances();
		
		List<Lance> resultado = filtro.filtra(Arrays.asList(
												new Lance(joao, 600),
												new Lance(joao, 500),
												new Lance(joao, 700),
												new Lance(joao, 800)));
		
		assertEquals(1, resultado.size());
		assertEquals(600, resultado.get(0).getValor(), 0.00001);
	}
	
	@Test
	public void deveSelecionaLancesAcima5000() {
		Usuario joao = new Usuario("joao");
		
		FiltroDeLances filtro = new FiltroDeLances();
		
		List<Lance> resultado = filtro.filtra(Arrays.asList(
												new Lance(joao, 1000),
												new Lance(joao, 500),
												new Lance(joao, 6000),
												new Lance(joao, 8000)));
		
		assertEquals(2, resultado.size());
		assertEquals(6000, resultado.get(0).getValor(), 0.00001);
		assertEquals(8000, resultado.get(1).getValor(), 0.00001);
	}
	
	@Test
	public void deveSelecionaLancesEntre700E1000() {
		Usuario joao = new Usuario("joao");
		
		FiltroDeLances filtro = new FiltroDeLances();
		
		List<Lance> resultado = filtro.filtra(Arrays.asList(
												new Lance(joao, 700),
												new Lance(joao, 750),
												new Lance(joao, 900),
												new Lance(joao, 800)));
		
		assertEquals(0, resultado.size());
	}
	
	@Test
	public void deveSelecionaLancesMenor500() {
		Usuario joao = new Usuario("joao");
		
		FiltroDeLances filtro = new FiltroDeLances();
		
		List<Lance> resultado = filtro.filtra(Arrays.asList(
												new Lance(joao, 400),
												new Lance(joao, 500),
												new Lance(joao, 300),
												new Lance(joao, 100)));
		
		assertEquals(0, resultado.size());
	}
	
	@Test
	public void deveSelecionaLancesEntre3000E5000() {
		Usuario joao = new Usuario("joao");
		
		FiltroDeLances filtro = new FiltroDeLances();
		
		List<Lance> resultado = filtro.filtra(Arrays.asList(
												new Lance(joao, 3500),
												new Lance(joao, 4500),
												new Lance(joao, 3575),
												new Lance(joao, 3200)));
		
		assertEquals(0, resultado.size());
	}
}
