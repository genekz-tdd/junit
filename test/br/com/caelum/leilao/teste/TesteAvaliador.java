package br.com.caelum.leilao.teste;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.dominio.Usuario;
import br.com.caelum.leilao.servico.Avaliador;

public class TesteAvaliador {

	@Test
	public void deveEntenderLancesEmOrdemCrescente() {
		Usuario joao = new Usuario("João");
		Usuario maria = new Usuario("Maria");
		Usuario jose = new Usuario("José");

		Leilao leilao = new Leilao("Playstation 3 novo");
		
		leilao.propoe(new Lance(joao, 300.00));
		leilao.propoe(new Lance(jose, 400.00));
		leilao.propoe(new Lance(maria, 500.00));
		
		Avaliador leiloeiro = new Avaliador();
		leiloeiro.avaliaMedia(leilao);
		
		double maiorEsperado = 500.00;
		double menorEsperado = 300.00;
		double mediaEsperada = 400.00;
		
		assertEquals(maiorEsperado, leiloeiro.getMaiorLance(), 0.00001);
		assertEquals(menorEsperado, leiloeiro.getMenorLance(), 0.00001);
		assertEquals(mediaEsperada, leiloeiro.getMedia(), 0.00001);
	}
	
	@Test
	public void deveEntenderLeilaoComApenasUmLance() {
		Usuario joao = new Usuario("João");
		
		Leilao leilao = new Leilao("Playstation 3 novo");
		
		leilao.propoe(new Lance(joao, 1000.00));
		
		Avaliador leiloeiro = new Avaliador();
		leiloeiro.avalia(leilao);
		
		assertEquals(1000.00, leiloeiro.getMaiorLance(), 0.00001);
		assertEquals(1000.00, leiloeiro.getMenorLance(), 0.00001);
	}

	@Test
	public void testaMediaDeZeroLance() {
		Usuario ewerton = new Usuario("Ewerton");
		
		Leilao leilao = new Leilao("IPhone 7");
		
		Avaliador avaliador = new Avaliador();
		avaliador.avaliaMedia(leilao);
		
		assertEquals(0, avaliador.getMedia(), 0.00001);
	}
	
	@Test
	public void deveEncontrarOsTresMaioresLances() {
		Usuario joao = new Usuario("João");
		Usuario maria = new Usuario("Maria");
		
		Leilao leilao = new Leilao("Playstation 3 novo");
		
		leilao.propoe(new Lance(joao, 100.00));
		leilao.propoe(new Lance(maria, 200.00));
		leilao.propoe(new Lance(joao, 300.00));
		leilao.propoe(new Lance(maria, 400.00));
		leilao.propoe(new Lance(joao, 500.00));
		
		Avaliador leiloeiro = new Avaliador();
		leiloeiro.avalia(leilao);
		
		List<Lance> maiores = leiloeiro.getTresMaiores();
		assertEquals(3, maiores.size());
		assertEquals(500.00, maiores.get(0).getValor(), 0.00001);
		assertEquals(400.00, maiores.get(1).getValor(), 0.00001);
		assertEquals(300.00, maiores.get(2).getValor(), 0.00001);
	}
}
